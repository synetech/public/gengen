import copy
import os
import shutil
from unittest import TestCase
from sources.modules.row_based_generator.model.user_input import UserInput
from sources.modules.row_based_generator.helpers.config_helper import ConfigHelper
from sources.modules.row_based_generator.generation_engine.rows_only_handler import RowsOnlyHandler
from sources.modules.row_based_generator.generation_engine.target_files_creator import TargetFilesCreator, \
    KeyValueStorageKey, KeyValueStorage
from tests.test_inputs import TestInputs
from sources.utils import containers


class RowsOnlyHandlerTests(TestCase):

    resources_folder = os.getcwd() + os.sep + 'Resources'

    def test(self):
        user_input = UserInput(copy.deepcopy(TestInputs.common_networking))
        template_folder_path = self.resources_folder + os.sep + 'TestTemplates' \
                               + os.sep + 'Structured' + os.sep + user_input.networking_type
        output_folder_path = self.resources_folder + os.sep + 'Output'
        config_file_path = self.resources_folder + os.sep + 'test_config_networking.conf'

        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, template_folder_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER, output_folder_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE, config_file_path)

        handler = containers.OBJECTS.provide(RowsOnlyHandler)
        target_file_creator = containers.OBJECTS.provide(TargetFilesCreator)

        config = ConfigHelper.get_config()
        target_file_creator.create_target_files_for_rows_only_input(user_input, config)

        handler.handle(user_input)

        shutil.rmtree(os.getcwd() + '/Resources/Output/CommonNetworking')