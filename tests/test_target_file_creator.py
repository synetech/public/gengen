import copy
import os
import shutil
from unittest import TestCase

from sources.modules.row_based_generator.helpers.config_helper import ConfigHelper
from sources.modules.row_based_generator.generation_engine.target_files_creator import TargetFilesCreator
from sources.modules.row_based_generator.key_value_storage import KeyValueStorage, KeyValueStorageKey
from sources.modules.row_based_generator.model.user_input import UserInput
from tests.Resources.MockConfig import MockConfig
from tests.test_inputs import TestInputs
from sources.utils.list_files import list_files


class TargetFileCreatorTests(TestCase):

    key_value_storage = KeyValueStorage()
    user_input = UserInput(TestInputs.some_cool_screen)
    creator = TargetFilesCreator()
    resources_folder = os.getcwd() + os.sep + 'Resources'

    def setUp(self):
        try:
            shutil.rmtree(os.getcwd() + '/Resources/Output/SomeCoolScreen')
        except FileNotFoundError:
            pass

    def tearDown(self):
        config_file_path = self.resources_folder + os.sep + 'test_config.conf'
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE, config_file_path)

    def test_creating_root_output_folder(self):
        self.creator.create_target_files_for_entire_user_input(self.user_input, MockConfig())
        output_path = self.key_value_storage.selected_output_folder
        directories = os.listdir(output_path)
        assert 'SomeCoolScreen' in directories
        shutil.rmtree(os.getcwd() + '/Resources/Output/SomeCoolScreen')

    def test_creating_folder_structure_according_to_template_folder_structure(self):
        template_folder_path = os.getcwd() + os.sep + 'Resources' + os.sep + 'TestTemplates' \
                               + os.sep + 'Structured' + os.sep + self.user_input.screen_name
        output_folder_path = os.getcwd() + os.sep + 'Resources' + os.sep + 'Output'
        created_folder_path = output_folder_path + os.sep + self.user_input.screen_name

        self.key_value_storage.set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, template_folder_path)
        self.key_value_storage.set_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER, output_folder_path)
        self.creator.create_target_files_for_entire_user_input(self.user_input, MockConfig())

        source_end_folders = set([test for test in list_files(template_folder_path)])
        source_end_folders = set([folder for folder in source_end_folders if 'SubTemplates' not in folder])
        source_end_folders = set([test.split('/')[-1] for test in source_end_folders])
        source_end_folders = set([file.replace('#screen_name#', 'SomeCoolScreen') for file in source_end_folders])

        target_end_folders = set([test for test in list_files(created_folder_path)])
        target_end_folders = set([folder for folder in target_end_folders if 'SubTemplates' not in folder])
        target_end_folders = set([test.split('/')[-1] for test in target_end_folders])
        target_end_folders = set([file.replace('#screen_name#', 'SomeCoolScreen') for file in target_end_folders])

        assert source_end_folders == target_end_folders
        shutil.rmtree(os.getcwd() + '/Resources/Output/SomeCoolScreen')

    def test_creating_rows_only_files(self):
        user_input = UserInput(copy.deepcopy(TestInputs.common_networking))

        template_folder_path = self.resources_folder + os.sep + 'TestTemplates' \
                               + os.sep + 'Structured' + os.sep + user_input.networking_type
        output_folder_path = self.resources_folder + os.sep + 'Output'
        config_file_path = self.resources_folder + os.sep + 'test_config_networking.conf'

        created_folder_path = output_folder_path + os.sep + user_input.networking_type

        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, template_folder_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER, output_folder_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE, config_file_path)

        config = ConfigHelper.get_config()
        self.creator.create_target_files_for_rows_only_input(user_input, config)

        target_end_files = set([test for test in list_files(created_folder_path)])
        target_end_files = [file.split(os.sep)[-1] for file in target_end_files]
        assert 'GetIdentityServerRequestSpec.swift' in target_end_files
        assert 'GetLoginStatusRequestSpec.swift' in target_end_files
        assert 'GetDeviceAccessTokenRequestSpec.swift' in target_end_files
        assert 'GetUserAccessTokenRequestSpec.swift' in target_end_files
        shutil.rmtree(os.getcwd() + '/Resources/Output/CommonNetworking')
