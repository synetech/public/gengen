import Swinject

extension DataSourceDependencies {

    static func register#networking_type#(to container: Container) {
        container.register(#networking_type#.self) { _ in
            #networking_type#Impl()
        }
    }
}
