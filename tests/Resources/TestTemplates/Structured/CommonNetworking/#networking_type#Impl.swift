import Moya
import RxSwift

public class #networking_type#Impl {

    // MARK: - Aliases
    typealias Endpoint = #networking_type#

    // MARK: - Properties
    private let provider: MoyaProvider<Endpoint> = {
        return MoyaProvider(endpointClosure: MoyaProvider.defaultEndpointMapping,
                            stubClosure: MoyaProvider.neverStub,
                            manager: MoyaProvider<Endpoint>.defaultAlamofireManager(),
                            plugins: [NetworkLoggerPlugin()],
                            trackInflights: false)
    }()

    // MARK: - Init
    public init() {}
}

extension #networking_type#Impl: #networking_type# {

    // TODO: add #networking_type# protocol implementation
}
