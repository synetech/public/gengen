19. There are some sub-templates with placeholders accepting top level input

struct SomeApiResponseModel: Codable {

    let firstProperty: String
    let secondProperty: Int
    let thirdProperty: Submodel

    enum CodingKeys: String, CodingKey {
        case firstProperty = "FirstProperty"
        case secondProperty = "SecondProperty"
        case thirdProperty = "ThirdProperty"
    }
}

extension SomeApiResponseModel {

    static var empty: SomeApiResponseModel {
        return Self(firstProperty: String.empty,
                    secondProperty: Int.empty,
                    thirdProperty: Submodel.empty)
    }
}
