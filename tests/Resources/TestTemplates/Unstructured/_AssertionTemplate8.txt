8. There is some sub-placeholder with a sub-placeholder while the first one is specified with if

extension SomeCoolScreenViewController {

     func bindbtnAddToSavedInteraction() {
         screenView.btnAddToSaved.tapGesture()
             .when(.recognized)
             .subscribe(onNext: { [weak self] _ in
                 // TODO: handle interactions
             })
             .disposed(by: disposeBag)
     }
}
