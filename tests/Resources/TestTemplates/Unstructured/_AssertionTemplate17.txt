17.

class SomeCoolScreenView {

    // MARK: - Views
    lazy var scroll = UIScrollView()
    lazy var scroll = UIScrollView()
    lazy var stack = UIStackView()
    lazy var content = UIView()

    lazy var imgProduct = UIImageView()
    lazy var btnAddToSaved = UIButton()
    lazy var lblName = UILabel()
    lazy var viewPrice = PriceView()

    // MARK: - Init
    init() {
        super.init(frame: .zero)

        setupViews()
        layoutBaseView()
        addContentViewSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    private func setupViews() {
        // TODO: add additional setup
    }

    private func layoutBaseView() {
      sv(scroll)
      scroll.sv(content)
      scroll.fillContainer()
      scroll.layout(0,
                    |content.width(100%)|,
                    0)
      content.sv(stack)
    }

    private func addContentViewSubviews() {
        stack.addArrangedSubviews(imgProduct,
                                  btnAddToSaved,
                                  lblName,
                                  viewPrice)
    }
}
