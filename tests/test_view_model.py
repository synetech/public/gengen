from unittest import TestCase
from sources.modules.row_based_generator.row_based_generator_config import RowBasedGeneratorConfig
from sources.modules.row_based_generator.ui.view_model import ViewModel
from sources.utils import containers


class ViewModelTests(TestCase):

    def test(self):
        view_model = containers.OBJECTS.provide(ViewModel)
        result = view_model.get_config()
        assert type(result) == RowBasedGeneratorConfig
        assert result.header == {'Screen name': {'id': 'screen_name', 'type': 'TextField'}}
        assert result.row == {'View name': {'id': 'name', 'type': 'TextField'}, 'View type': {'id': 'view_type', 'type': 'DropDownMenu', 'options': ['UIView', 'UILabel', 'UIButton', 'UIImageView', 'UITableView', 'UICollectionView', 'UIScrollView', 'UIStackView', 'UITextField', 'UITextView', 'WKWebView', 'Custom']}, 'Custom type': {'id': 'custom_type', 'type': 'TextField'}, 'Data': {'id': 'data_entry', 'type': 'TextField'}, 'Data type': {'id': 'data_type', 'type': 'TextField'}, 'Interaction': {'id': 'interaction', 'type': 'Checkbox'}, 'Reused': {'id': 'reused', 'type': 'Checkbox'}}
        assert result.output_folder_name_key == 'screen_name'
