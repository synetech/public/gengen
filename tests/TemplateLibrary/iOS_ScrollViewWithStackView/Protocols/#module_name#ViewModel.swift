import RxSwift

protocol #module_name#ViewModel {

    func getModel() -> Observable<#module_name#ScreenModel>
}
