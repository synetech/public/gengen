func bind#name#Interaction() {
    screenView.#name#.tapGesture()
        .when(.recognized)
        .subscribe(onNext: { [weak self] _ in
            // TODO: handle interactions
        })
        .disposed(by: disposeBag)
}