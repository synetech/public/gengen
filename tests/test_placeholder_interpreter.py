import copy
from unittest import TestCase
from sources.modules.row_based_generator.model.user_input import UserInput
from sources.modules.row_based_generator.generation_engine.placeholder_interpreter import PlaceholderInterpreter
from tests.test_inputs import *


class PlaceholderInterpreterTests(TestCase):

    placeholder_interpreter = PlaceholderInterpreter

    """
    tests that when a placeholder with a specification (i.e. '.if_has(data_entry)') is passed, 
    the placeholder's values are properly passed and returned as a dictionary.
    """
    def test_parse_placeholder(self):
        result = self.placeholder_interpreter.parse_placeholder("data_binding.if_has(data_entry)")
        assert result == {
            'placeholder': 'data_binding',
            'specifier': 'if_has',
            'argument': 'data_entry'
        }

    """
    tests that when a placeholder is specified with 'switch_if_provided' specification, the placeholder is properly
    adjusted to match the specification.
    """
    def test_switch_if_provided(self):
        input_copy = copy.deepcopy(TestInputs.some_cool_screen)
        result = self.placeholder_interpreter.adjust_placeholder("#view_type.switch_if_provided(custom_type)#",
                                                                 UserInput(input_copy).rows[3])
        assert result == '#custom_type#'

        result = self.placeholder_interpreter.adjust_placeholder("#view_type.switch_if_provided(custom_type)#",
                                                                 UserInput(input_copy).rows[0])
        assert result == '#view_type#'
