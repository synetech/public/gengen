import copy
from unittest import TestCase
from sources.modules.row_based_generator.generation_engine.row_input_adjuster import RowInputAdjuster
from sources.modules.row_based_generator.key_value_storage import *
from sources.utils import util_functions
from sources.modules.row_based_generator.model.user_input import UserInput
from tests.test_inputs import *
from sources.modules.row_based_generator.generation_engine.placeholder_replacer import PlaceholderReplacer
from sources.modules.row_based_generator.generation_engine.sub_placeholder_replacer import SubPlaceholderReplacer


class PlaceholderReplacerTests(TestCase):

    """
    For every test, there needs to be two files in Resources/TestTemplates/Unstructured:
    1) File with naming convention _TestTemplate<TEST_NUMBER>.txt that serves as the input for PlaceholderReplacer
    2) File with naming convention _AssertionTemplate<TEST_NUMBER>.txt that serves as validation

    Every test proceeds as follows:
    1) The content of the test file is passed to PlaceholderReplacer
    2) PlaceholderReplacer does its job and returns the result
    3) The result is checked with the content of the assertion file

    If there is a mismatch, a log is printed containing the expected content and the actual content.
    For further improvement, we can add indication for particular lines, that differ.

    NOTE: these tests could run in a for loop, but for the purpose of debugging,
    it's more practical to run them one by one
    """
    test = copy.deepcopy(TestInputs.some_cool_screen)
    template_folder = os.getcwd() + os.sep + 'Resources/TestTemplates'

    def tearDown(self):
        self.test = copy.deepcopy(TestInputs.some_cool_screen)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE, 'Resources/test_config.conf')

    def test_top_level_values_replacement(self):
        self.run_test(1)

    def test_iteratively_produced_data_for_sub_placeholder(self):
        self.run_test(2)

    def test_list(self):
        self.run_test(3)

    def test_list_with_content_between_rows(self):
        self.run_test(4)

    def test_sub_placeholder_with_top_level_input(self):
        self.run_test(5)

    def test_sub_placeholder_with_sub_placeholder(self):
        self.run_test(6)

    def test_sub_placeholder_with_sub_placeholder_with_if_has_specification_1(self):
        self.run_test(7)

    def test_sub_placeholder_with_sub_placeholder_with_if_has_specification_2(self):
        self.run_test(8)

    def test_sub_placeholder_with_sub_placeholder_with_if_has_specification_3(self):
        self.run_test(9)

    def test_if_has_specification_referring_to_different_input_values(self):
        self.run_test(10)

    def test_if_has_specification_referring_to_same_input_values(self):
        self.run_test(11)

    def test_if_some_row_has_specification(self):
        self.run_test(12)

    def test_sub_template_without_any_placeholder(self):
        self.run_test(13)

    def test_multiple_placeholder_containing_subplaceholders(self):
        self.run_test(14)

    def test_multiple_placeholder_containing_sub_placeholder(self):
        self.run_test(15)

    def test_multiple_placeholder_containing_sub_placeholder_extended(self):
        self.run_test(16)

    def test_complete_view(self):
        self.run_test(17)

    def test_mupltiple_placeholders_in_sub_template(self):
        self.test = copy.deepcopy(TestInputs.api_response_model)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE,
                                    'Resource/test_config_api_response_model.conf')
        self.run_test(18)

    def test_placeholder_in_sub_template_accepting_top_level_value(self):
        self.test = copy.deepcopy(TestInputs.api_response_model)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE,
                                    'Resource/test_config_api_response_model.conf')
        self.run_test(19)

    def test_placeholder_if_specifier_on_template_top_level(self):
        self.test = copy.deepcopy(TestInputs.product_model)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE,
                                    'Resource/test_config_bridged_entities.conf')
        self.run_test(20)

    def test_placeholder_with_specifier_lowercased(self):
        self.test = copy.deepcopy(TestInputs.all_creatures)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE,
                                    'Resource/test_config_bridged_entities.conf')
        self.run_test(21)

    def test_list_with_content_before_first_placeholder(self):
        self.test = copy.deepcopy(TestInputs.all_creatures)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE,
                                    'Resource/test_config_recyclerview_uitableview.conf')
        self.run_test(22)

    def run_test(self, index):
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, self.template_folder)

        template = util_functions.read_file(self.template_folder + os.sep + 'Unstructured' + os.sep
                                            + '_TestTemplate' + str(index) + '.txt')

        user_input = UserInput(self.test)
        replacer = PlaceholderReplacer(SubPlaceholderReplacer(), RowInputAdjuster())
        result = replacer.process(template, user_input)
        self.check_result(index, result)

    def check_result(self, index, result):
        assertion = util_functions.read_file(self.template_folder + os.sep + 'Unstructured' + os.sep
                                             + '_AssertionTemplate' + str(index) + '.txt')
        if result != assertion:
            self.print_test_fail_log(assertion, result)
            assert False

    @staticmethod
    def print_test_fail_log(assertion, result):
        print('= ' * 30)
        print('EXPECTED')
        print('= ' * 30)
        print(assertion)
        print('= ' * 30)
        print()
        print('= ' * 30)
        print('ACTUAL')
        print('= ' * 30)
        print(result)
        print('= ' * 30)

