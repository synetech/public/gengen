class UserInput:
    """
    Serves to represent user input for generation handling
    All values are accessible either as a class property
    (by modifying the '__dict__' property on init)
    or as the original dictionary through 'data' property.
    """

    def __init__(self, inputs):
        """
        :param inputs: dict containing user input values retrieved from the ui
        """
        self.data = inputs
        self.rows = inputs['rows']
        for key in inputs.keys():
            self.__dict__[key] = inputs[key]
