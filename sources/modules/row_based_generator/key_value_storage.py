import ast
import os
import sys
from enum import Enum
from sources.utils import util_functions


class KeyValueStorageKey(Enum):

    TEST_KEY = 'test_key'
    SELECTED_TEMPLATE_FOLDER = 'selected_template_folder'
    SELECTED_OUTPUT_FOLDER = 'selected_output_folder'
    SELECTED_CONFIG_FILE = 'selected_config_file'
    LAST_OPENED_INPUT_FILE = 'last_opened_input_file'


class KeyValueStorage:

    selected_template_folder = ''
    selected_output_folder = ''
    data_file_name = 'KeyValueStorageData.txt'

    @property
    def data_file_path(self):
        if getattr(sys, 'frozen', False):
            return os.path.dirname(sys.executable) + os.sep + self.data_file_name
        else:
            return os.getcwd() + os.sep + self.data_file_name

    def __init__(self):
        self.__load_data()

    def __load_data(self):
        try:
            data = util_functions.read_file(self.data_file_path)
        except FileNotFoundError:
            util_functions.write_to_file(self.data_file_path, '{}')
            data = None
        if data:
            dictionary = ast.literal_eval(data)
            self.__dict__ = dictionary
        else:
            util_functions.write_to_file(self.data_file_path, '{}')

    def set_value(self, key, value):
        data = util_functions.read_file(self.data_file_path)
        dictionary = ast.literal_eval(data)
        dictionary[key.value] = value
        util_functions.write_to_file(self.data_file_path, dictionary)
        self.__load_data()

    def get_value(self, key):
        if key.value in self.__dict__:
            return self.__dict__[key.value]
        else:
            raise ValueForKeyNotFound

    def delete_key(self, key):
        data = util_functions.read_file(self.data_file_path)
        dictionary = ast.literal_eval(data)
        if key.value in self.__dict__:
            del self.__dict__[key.value]
        else:
            # TODO: raise error here if more suitable
            return str(key) + ' NOT FOUND'
        util_functions.write_to_file(self.data_file_path, dictionary)


class ValueForKeyNotFound(Exception):
    pass
