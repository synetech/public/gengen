from sources.modules.row_based_generator.key_value_storage import *

default_config = """
{
    'header': {
        "Some header field": {
            "id": "header",
            "type": 'TextField'
        }
    },

    'row': {
        "Some row field": {
            "id": "row_field",
            "type": "TextField"
        },
    },

    'output_folder_name_key': 'header',

    'row_rules': [],

    "rows_only": []
}
""".strip()


class RowBasedGeneratorConfig:

    def __init__(self, config_file_content=default_config, templates=['TEMPLATE NOT SELECTED']):
        if config_file_content:
            dictionary = ast.literal_eval(config_file_content)
            self.__dict__ = dictionary
            self.__dict__['templates'] = templates
            try:
                selected_template_path = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER)
                selected_template = selected_template_path.split(os.sep)[-1]
                self.__dict__['selected_template'] = selected_template
            except ValueForKeyNotFound:
                self.__dict__['selected_template'] = templates[0]
