from tkinter import Widget


class Component:

    id = str
    component_type = Widget

    def show(self, column, row):
        self.component_type.grid(column=column, row=row, pady=(5, 5))

    def hide(self):
        self.component_type.grid_forget()

    def get_id(self):
        return self.id
