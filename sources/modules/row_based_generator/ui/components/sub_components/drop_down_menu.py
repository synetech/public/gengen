from rx.subject.subject import Subject
from tkinter import OptionMenu, StringVar
from sources.modules.row_based_generator.ui.components.sub_components.component import Component


class DropDownMenuComponent(Component):

    current_value = Subject()

    def __init__(self, window, id, *options):
        self.window = window
        self.id = id
        self.options = options
        self.selected_value = StringVar(window)
        self.selected_value.set(options[0])

        self.component_type = OptionMenu(window, self.selected_value,
                                         *self.options)
    def set_action(self, action):
        self.component_type = OptionMenu(self.window, self.selected_value,
                                         *self.options,
                                         command=lambda x: action(self.selected_value.get()))

    def set_value(self, value):
        self.selected_value.set(value)

    def get_value(self):
        return self.selected_value.get()

    def bind_to(self, callback):
        self.selected_value.trace('w', lambda x, y, z: callback(self.get_value()))
