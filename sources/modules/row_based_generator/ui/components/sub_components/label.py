from tkinter import Label
from sources.modules.row_based_generator.ui.components.sub_components.component import Component


class LabelComponent(Component):

    def __init__(self, window, label):
        self.window = window
        self.id = label
        self.component_type = Label(master=self.window, text=label)

    def set_text(self, text):
        self.component_type.configure(text=text)
