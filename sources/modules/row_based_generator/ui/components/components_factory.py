from sources.modules.row_based_generator.ui.components.sub_components.checkbox import CheckboxComponent as Checkbox
from sources.modules.row_based_generator.ui.components.sub_components.drop_down_menu import DropDownMenuComponent as DropDownMenu
from sources.modules.row_based_generator.ui.components.sub_components.text_field import TextFieldComponent as TextField


class ComponentsFactory:

    def __init__(self, view_row_config):
        self.view_row_config = view_row_config

    def create_view_row_components(self, window):
        components = []
        for key in self.view_row_config.row:
            view_config = self.view_row_config.row[key]
            self.__get_component(components, view_config, window)
        return components

    def create_main_header_components(self, window):
        components = []
        for key in self.view_row_config.header:
            view_config = self.view_row_config.header[key]
            self.__get_component(components, view_config, window)
        return components

    def __get_component(self, components, view_config, window):
        view = view_config['type']
        # TODO: if more input view types are added, add them here too
        if view == "TextField":
            components.append(TextField(window, id=view_config['id']))
        elif view == "DropDownMenu":
            components.append(DropDownMenu(window, view_config['id'], *view_config['options']))
        elif view == "Checkbox":
            components.append(Checkbox(window, id=view_config['id']))
