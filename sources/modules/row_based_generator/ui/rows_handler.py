from sources.modules.row_based_generator.ui.components.rows_section.row import Row


class RowsHandler:

    """ Init """
    def __init__(self):
        self.rows = []
        self.current_parent_view = self
        self.currently_edited_view = self

    """ Public """
    def add_row(self, view_handler):
        next_row_index = len(self.currently_edited_view.rows)
        view_row = Row(row=next_row_index, view_handler=view_handler)

        if self.currently_edited_view == self:
            self.rows.append(view_row)
        else:
            self.currently_edited_view.add_row(view_row)

    def remove_view_row(self, view_row):
        view_row.hide()
        self.__find_parent_view(self.currently_edited_view, view_row)
        parent_view = self.current_parent_view
        index = parent_view.rows.index(view_row)
        parent_view.rows[index].hide()
        del parent_view.rows[index]
        self.__update_indexes()

    def show_subviews(self, view):
        self.__find_parent_view(self, view)
        parent_view = self.current_parent_view
        for view_row in parent_view.rows:
            view_row.hide()

        self.currently_edited_view = view

        for added_subview in view.rows:
            added_subview.show()

    def show_superviews(self):
        self.__find_parent_view(self, self.currently_edited_view)
        parent_view = self.current_parent_view
        for view in self.currently_edited_view.rows:
            view.hide()
        for view in parent_view.rows:
            view.show()
        self.currently_edited_view = parent_view

    def process_loaded_data(self, loaded_views, view_handler):
        self.__erase_current_rows()
        self.__update_rows(loaded_views, view_handler)
        for row in self.rows:
            row.show()

    def currently_editing_topmost_view(self) -> bool:
        return self.currently_edited_view == self.current_parent_view

    def get_input(self):
        return [row.to_dictionary() for row in self.rows]

    def remove_all_rows(self):
        for row in self.rows:
            row.hide()
        self.rows = []

    """ Private """
    def __erase_current_rows(self):
        for row in self.rows:
            row.hide()
        self.rows = []

    def __update_rows(self, loaded_views, view_handler):
        for view_data_entry in loaded_views:
            next_row_index = len(self.rows)
            view_row = Row.from_dictionary(view_data_entry, next_row_index, view_handler)
            self.rows.append(view_row)

    def __update_indexes(self):
        parent_view = self.current_parent_view
        # TODO: consider other way to update indexes, e.g. changing just the number labels
        for index in range(0, len(parent_view.rows)):
            parent_view.rows[index].hide()
            parent_view.rows[index].update_row_index(index)
            parent_view.rows[index].show()

    def __find_parent_view(self, view, searched_view):
        if searched_view in view.rows:
            self.current_parent_view = view
        else:
            for row in view.rows:
                self.__find_parent_view(row, searched_view)
