from enum import Enum
from tkinter import Tk, Grid, filedialog, Frame
from rx.subject.subject import Subject
from sources.utils import version
from sources.utils.keys import Keys
from .components.rows_section.row import Row
from .components.main_menu import MainMenu
from .components.header import Header
from .tree_view_visualizer import TreeViewVisualizer
from .components.rows_section.rows_section_header import RowsSectionHeader


class Labels:

    window_title = "GenGen v" + version.NUMBER
    back_button_title = "Back"


class UIEvent(Enum):

    GENERATE_BUTTON_TAPPED = 'generate_button_tapped'


class MainViewHandler:

    event_subject = Subject()

    """ Init """
    def __init__(self, rows_handler, view_model):
        self.rows_handler = rows_handler
        self.view_model = view_model
        self.config = view_model.get_config()
        self.main_window = Tk()
        self.main_menu = None
        self.view_rows_header_view = None
        self.header_view = None

        self.__setup_frames()
        self.__setup_views()

        # TEST
        self.view_model.get_config()

    """ Setup """
    def __setup_main_window(self):
        screen_width = self.main_window.winfo_screenwidth()
        screen_height = self.main_window.winfo_screenheight()

        Grid.rowconfigure(self.main_window, 0, weight=0)
        Grid.columnconfigure(self.main_window, 1, weight=1)
        self.main_window.geometry(f"{screen_width}x{screen_height}")
        self.main_window.title(Labels.window_title)
        self.main_window['padx'] = 20

    def __setup_main_menu(self):
        self.main_menu = MainMenu(self, self.view_model.get_config())
        self.main_menu.show()

    def __setup_header_views(self):
        if self.header_view:
            self.header_view.hide()
        self.header_view = Header(self.header_frame, self.view_model.get_config())
        self.header_view.show()
        if self.view_rows_header_view:
            self.view_rows_header_view.hide()
        self.view_rows_header_view = RowsSectionHeader(self.rows_frame, self, self.view_model.get_config())
        self.view_rows_header_view.show()

    def __setup_default_view_row(self):
        self.rows_handler.remove_all_rows()
        default_row = Row(row=0, view_handler=self)
        self.rows_handler.rows.append(default_row)

    def __setup_frames(self):
        self.main_menu_frame = Frame(self.main_window)
        self.main_menu_frame.grid(row=0, column=0, columnspan=10)
        self.header_frame = Frame(self.main_window)
        self.header_frame.grid(row=1, column=0, columnspan=10)
        self.rows_frame = Frame(self.main_window)
        self.rows_frame.grid(row=2, column=0, columnspan=10)

    def __setup_views(self):
        self.__setup_main_window()
        self.__setup_main_menu()
        self.__setup_header_views()
        self.__setup_default_view_row()

    def __setup_dynamic_views(self):
        self.config = self.view_model.get_config()
        self.__setup_header_views()
        self.__setup_default_view_row()

    """ Public """
    def run(self):
        self.main_window.mainloop()

    def on_add_subview_button_click(self):
        self.rows_handler.add_row(view_handler=self)

    def on_generate_button_click(self):
        inputs = self.header_view.get_input()
        inputs[Keys.rows] = self.rows_handler.get_input()
        self.event_subject.on_next((UIEvent.GENERATE_BUTTON_TAPPED, inputs))

    def on_show_subviews_button_click(self, view):
        leaving_main_view = self.rows_handler.currently_editing_topmost_view()
        if leaving_main_view:
            self.header_view.hide()
            self.view_rows_header_view.show_back_button()

        self.rows_handler.show_subviews(view)

    def on_back_button_click(self):
        self.rows_handler.show_superviews()
        going_back_to_main_view = self.rows_handler.current_parent_view == self.rows_handler
        self.__update_header_view(will_show_main_view=going_back_to_main_view)

    def on_remove_button_click(self, view):
        self.rows_handler.remove_view_row(view)

    def on_load_button_click(self):
        file_path = filedialog.askopenfilename(initialdir=self.view_model.get_last_opened_input_directory(),
                                               title="Select file",
                                               filetypes=(("DICT files", "*.dict"),
                                                          ("all files", "*.*")))
        loaded_views = self.view_model.on_input_file_change(file_path)

        self.__setup_dynamic_views()

        if loaded_views != {}:
            self.header_view.set_input(loaded_views)
            self.rows_handler.process_loaded_data(loaded_views[Keys.rows], self)

    def on_save_button_click(self):
        inputs = self.header_view.get_input()
        rows = self.rows_handler.get_input()
        inputs[Keys.rows] = rows
        file_path = filedialog.asksaveasfilename(initialdir=self.view_model.get_last_opened_input_directory(),
                                                 title="Select file",
                                                 filetypes=(("DICT files", "*.dict"),
                                                            ("all files", "*.*")))
        if file_path:
            self.view_model.on_save_button_click(inputs, file_path)

    def on_visualize_button_click(self):
        TreeViewVisualizer.show_tree_view(self.rows_handler.rows)

    def on_templates_button_click(self):
        file_path = filedialog.askdirectory(initialdir=self.view_model.get_selected_template_folder())
        if file_path == '':
            return
        self.view_model.on_templates_folder_selected(file_path)

    def on_config_file_button_click(self):
        file_path = filedialog.askopenfilename(initialdir=self.view_model.get_selected_config_file())
        if file_path == '':
            return
        self.view_model.on_config_file_change(file_path)
        self.config = self.view_model.get_config()

        self.__setup_header_views()
        self.__setup_default_view_row()
        self.main_menu.set_selected_config_file_label(file_path)

    def on_output_dir_button_click(self):
        dir_path = filedialog.askdirectory(initialdir=self.view_model.get_selected_output_folder())
        if dir_path:
            self.view_model.on_output_directory_change(dir_path)

    def on_output_button_click(self):
        self.view_model.on_ouput_button_click()

    def get_inputs(self):
        inputs = self.header_view.get_input()
        inputs[Keys.rows] = self.rows_handler.get_input()
        return inputs

    def on_template_change(self, selected_value):
        self.view_model.on_template_change(selected_value)
        self.__setup_dynamic_views()

    def __update_header_view(self, will_show_main_view):
        if will_show_main_view:
            self.header_frame.grid(column=0, row=1)
            self.header_view.show()
            self.view_rows_header_view.show()
            self.view_rows_header_view.hide_back_button()
        else:
            self.header_frame.grid(column=0, row=1)
            self.header_view.show()
            self.header_view.hide()
            self.view_rows_header_view.show_back_button()
