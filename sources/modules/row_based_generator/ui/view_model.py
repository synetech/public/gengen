import subprocess
import os
from sources.modules.row_based_generator.helpers.config_helper import ConfigHelper
from sources.modules.row_based_generator.key_value_storage import *


class ViewModel:

    def __init__(self):
        self.rows = []
        self.config = None
        self.init_config()

    def init_config(self):
        self.config = ConfigHelper.get_config()

    def get_config(self):
        return self.config

    def on_config_file_change(self, file_path):
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE, file_path)
        self.init_config()

    def on_output_directory_change(self, dir_path):
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER, dir_path)

    def on_input_file_change(self, file_path):
        content = util_functions.read_file(file_path)
        content = ast.literal_eval(content)
        selected_template = content['selected_template']
        template_folder_path = os.getcwd() + os.sep + 'TemplateLibrary' + os.sep + selected_template
        files = os.listdir(template_folder_path)
        config_file = [file for file in files if '.config' in file][0]
        config_file_path = template_folder_path + os.sep + config_file

        KeyValueStorage().set_value(KeyValueStorageKey.LAST_OPENED_INPUT_FILE, file_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE, config_file_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, template_folder_path)
        self.init_config()
        return content

    def on_save_button_click(self, user_input, file_path):
        selected_template_file = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER)
        selected_template = selected_template_file.split(os.sep)[-1]
        user_input['selected_template'] = selected_template
        util_functions.write_to_file(file_path, user_input)

    def on_templates_folder_selected(self, file_path):
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, file_path)

    def on_ouput_button_click(self):
        try:
            folder = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER)
        except ValueForKeyNotFound:
            folder = os.getcwd() + os.sep + 'Output'
        subprocess.call(['open', folder])

    def get_selected_template_folder(self):
        return KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER)

    def get_selected_config_file(self):
        return KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_CONFIG_FILE)

    def get_last_opened_input_directory(self):
        try:
            file = KeyValueStorage().get_value(KeyValueStorageKey.LAST_OPENED_INPUT_FILE)
            return os.sep.join(file.split(os.sep)[:-1])
        except ValueForKeyNotFound:
            return os.getcwd() + os.sep + 'ExampleInputs'

    def get_selected_output_folder(self):
        try:
            return KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER)
        except ValueForKeyNotFound:
            return os.getcwd() + os.sep + 'Output'

    def on_template_change(self, value):
        template_folder_path = util_functions.get_working_directory() + os.sep + 'TemplateLibrary' + os.sep + value
        try:
            files = os.listdir(template_folder_path)
        except FileNotFoundError:
            files = ['NO TEMPLATE SELECTED']
        files = [file for file in files if file != '.DS_Store']
        try:
            config_file = [file for file in files if '.config' in file][0]
            config_file_path = template_folder_path + os.sep + config_file
        except IndexError:
            config_file_path = ''
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, template_folder_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE, config_file_path)

        self.init_config()
