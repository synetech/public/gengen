from .placeholder_interpreter import PlaceholderInterpreter
from .placeholder_list_handler import PlaceholderListHandler
from sources.modules.row_based_generator.key_value_storage import *
from .placeholder_processor import PlaceholderProcessor
from sources.utils import util_functions


class SubPlaceholderReplacer(PlaceholderProcessor):

    def process(self, sub_placeholders, template, user_input):
        sub_placeholders_with_values = self.__get_sub_placeholders_with_values(sub_placeholders, user_input)
        return self.__replace_placeholders_with_values(sub_placeholders_with_values, template)

    def __get_sub_placeholders_with_values(self, sub_placeholders, user_input):
        sub_placeholders_with_values = {}
        for sub_placeholder_key in sub_placeholders:
            sub_placeholder = sub_placeholders[sub_placeholder_key]
            placeholders = self.get_all_placeholders(sub_placeholder)
            result = self.__process_sub_placeholders(placeholders, sub_placeholder, user_input)
            sub_placeholders_with_values[sub_placeholder_key] = result
        return sub_placeholders_with_values

    def __process_sub_placeholders(self, placeholders, sub_placeholder, user_input):
        processed_sub_placeholder \
            = self.__replace_placeholders_with_top_level_values(placeholders,
                                                                sub_placeholder,
                                                                user_input)

        if '...' in sub_placeholder:
            return PlaceholderListHandler().handle_placeholder(processed_sub_placeholder, user_input.rows)

        if processed_sub_placeholder != sub_placeholder \
                or self.get_all_placeholders(processed_sub_placeholder) == []:
            return processed_sub_placeholder
        placeholders = self.get_all_placeholders(processed_sub_placeholder)
        result = self.__replace_placeholders_with_row_data_values(placeholders, processed_sub_placeholder, user_input)
        return result.strip()

    def __replace_placeholders_with_top_level_values(self, placeholders, sub_placeholder, user_input):
        processed_sub_placeholder = sub_placeholder
        for placeholder in placeholders:
            placeholder_id = placeholder[1:-1]
            if placeholder_id in user_input.data:
                processed_sub_placeholder = sub_placeholder.replace(placeholder, user_input.data[placeholder_id])
        return processed_sub_placeholder

    def __replace_placeholders_with_row_data_values(self, placeholders, sub_placeholder, user_input):
        result = ''
        for row in user_input.rows:
            processed_sub_placeholder = sub_placeholder
            for placeholder in placeholders:
                processed_sub_placeholder \
                    = self.__process_placeholder(placeholder, processed_sub_placeholder, row)
            result += processed_sub_placeholder + '\n'
        return result

    def __process_placeholder(self, placeholder, processed_sub_placeholder, row):
        if r'\_' in processed_sub_placeholder:
            processed_sub_placeholder = processed_sub_placeholder.replace(r'\_', ' ')
        try:
            _, processed_sub_placeholder \
                = self.__replace_placeholder(placeholder,
                                             processed_sub_placeholder, row)
        except KeyError:
            processed_sub_placeholder \
                = self.__process_placeholder_in_terminal_sub_template(placeholder,
                                                                      processed_sub_placeholder,
                                                                      row)
        return processed_sub_placeholder

    def __process_placeholder_in_terminal_sub_template(self, placeholder, processed_sub_placeholder, row):
        # TODO: cleanup
        data_key = self.__get_data_key(placeholder, row)
        if data_key:
            terminal_sub_template = self.__get_terminal_sub_template(data_key)
        else:
            terminal_sub_template = ''
        new_sub_placeholders = self.get_all_placeholders(terminal_sub_template)

        for new_sub_placeholder in new_sub_placeholders:
            data_key = new_sub_placeholder[1:-1]
            if '.capitalized' in data_key:
                data_key = data_key.split('.')[0]
                value = row[data_key][0].upper() + row[data_key][1:]
                terminal_sub_template = terminal_sub_template.replace(new_sub_placeholder, value)
            else:
                terminal_sub_template = terminal_sub_template\
                    .replace(new_sub_placeholder, row[data_key])
        processed_sub_placeholder = processed_sub_placeholder\
            .replace(placeholder, terminal_sub_template)
        return processed_sub_placeholder

    def __replace_placeholder(self, placeholder, processed_sub_placeholder, row):
        data_key = self.__get_data_key(placeholder, row)
        # TODO: implement row adjustment in placeholder interpreter to make this generic
        if 'capitalized' in data_key:
            data_key = data_key.split('.')[0]
        value = row[data_key]
        value = value[:1].upper() + value[1:] if 'capitalized' in placeholder else row[data_key]
        processed_sub_placeholder = processed_sub_placeholder.replace(placeholder, value)
        return data_key, processed_sub_placeholder

    def __get_data_key(self, placeholder, row):
        if '.' in placeholder:
            interpreted = PlaceholderInterpreter.adjust_placeholder(placeholder, row)
            if not interpreted:
                return ''
            data_key = interpreted[1:-1]
        else:
            data_key = placeholder[1:-1]
        return data_key

    @classmethod
    def __get_terminal_sub_template(cls, data_key):
        # TODO: remove duplicity
        sub_templates_file = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER) + os.sep \
                               + 'SubTemplates' + os.sep + data_key + '.txt'
        # TODO: add error
        sub_template = util_functions.read_file(sub_templates_file)
        return sub_template

    def __replace_placeholders_with_values(self, sub_placeholders_with_values, template):
        for sub_placeholder_key in sub_placeholders_with_values:
            target_value = self.__match_indentation(sub_placeholder_key, sub_placeholders_with_values, template)
            template = template.replace(sub_placeholder_key, target_value)
        return template

    @classmethod
    def __match_indentation(cls, sub_placeholder_key, sub_placeholders_with_values, template):
        template_rows = template.split('\n')
        target_row = [row for row in template_rows if sub_placeholder_key in row][0]
        split_target_row = target_row.split(sub_placeholder_key)[0]
        whitespaces_count = len(split_target_row) - len(split_target_row.lstrip())
        split_target_value = sub_placeholders_with_values[sub_placeholder_key].split('\n')
        for index in range(1, len(split_target_value)):
            split_target_value[index] = ' ' * whitespaces_count + split_target_value[index]
        target_value = '\n'.join(split_target_value).rstrip()
        return target_value
