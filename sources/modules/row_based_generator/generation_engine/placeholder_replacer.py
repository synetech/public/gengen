from .placeholder_processor import PlaceholderProcessor
from sources.modules.row_based_generator.key_value_storage import *


class PlaceholderReplacer(PlaceholderProcessor):

    def __init__(self, sub_placeholder_replacer, row_input_adjuster):
        self.sub_placeholder_replacer = sub_placeholder_replacer
        self.row_input_adjuster = row_input_adjuster
        self.template = None

    def process(self, template, user_input):
        template = self.__process(template, user_input)
        remaining_placeholders = self.get_all_placeholders(template)
        if remaining_placeholders:
            return self.process(template, user_input)

        return template

    def __process(self, template, user_input):
        not_found_placeholders = []

        template = self.row_input_adjuster.interpret_top_level_specification(template, user_input)
        template = self.__process_top_level_placeholders(not_found_placeholders, template, user_input)
        template = self.__process_sub_placeholders(not_found_placeholders, template, user_input)
        template = self.__remove_trailing_whitespaces(template)
        return template

    def __process_sub_placeholders(self, not_found_placeholders, template, user_input):
        if not_found_placeholders:
            for placeholder in not_found_placeholders:
                template = self.__process_not_found_placeholder(placeholder, template, user_input)
        return template

    def __process_top_level_placeholders(self, not_found_placeholders, template, user_input):
        placeholders = self.get_all_placeholders(template)
        for placeholder in placeholders:
            try:
                data_key = placeholder[1:-1]
                if '.capitalized' in placeholder:
                    data_key = data_key.split('.')[0]
                    value = user_input.data[data_key]
                    value = value[0].upper() + value[1:]
                    template = template.replace(placeholder, value)
                    continue
                if '.lowercased' in placeholder:
                    data_key = data_key.split('.')[0]
                    value = user_input.data[data_key]
                    template = template.replace(placeholder, value.lower())
                    continue
                template = template.replace(placeholder, user_input.data[placeholder[1:-1]])
            except KeyError:
                not_found_placeholders.append(placeholder)
        return template

    def __process_not_found_placeholder(self, placeholder, template, user_input):
        sub_placeholders = self.__get_sub_templates_for_placeholders([placeholder], user_input)
        if 'user_input' in sub_placeholders:
            processed_user_input = sub_placeholders['user_input']
            del sub_placeholders['user_input']
            template = self.sub_placeholder_replacer.process(sub_placeholders,
                                                             template, processed_user_input)
        else:
            template = self.sub_placeholder_replacer.process(sub_placeholders,
                                                             template, user_input)
        return template

    def __get_sub_templates_for_placeholders(self, placeholders, user_input):
        results = {}
        sub_templates_folder = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER) \
                               + os.sep + 'SubTemplates'
        for placeholder in placeholders:
            file_name = self.__process_placeholder(placeholder, results, user_input)
            # TODO: handle file not found error
            results[placeholder] = util_functions.read_file(sub_templates_folder + os.sep + file_name)
        return results

    def __process_placeholder(self, placeholder, results, user_input):
        if '.' in placeholder and placeholder:
            file_name = self.__process_specified_placeholder(placeholder, results, user_input)
        else:
            file_name = placeholder[1:-1] + '.txt'
        return file_name

    def __process_specified_placeholder(self, placeholder, results, user_input):
        results['user_input'] = self.row_input_adjuster \
            .adjust_user_input_for_placeholder(placeholder, user_input)
        placeholder_root = placeholder.split('.')[0][1:]
        file_name = placeholder_root + '.txt'
        return file_name

    @classmethod
    def __remove_trailing_whitespaces(cls, template):
        lines_without_trailing_whitespace = [line.rstrip() for line in template.split('\n')]
        return '\n'.join(lines_without_trailing_whitespace)
