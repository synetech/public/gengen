""" PlaceholderListHandler """
import re
from sources.modules.row_based_generator.generation_engine.placeholder_processor \
    import PlaceholderProcessor


class PlaceholderListHandler(PlaceholderProcessor):

    def handle_placeholder(self, list_placeholder, user_input_rows):
        placeholders = set(self.get_all_placeholders(list_placeholder))
        placeholder_rows = [row for row in list_placeholder.split('\n') if row]

        result = self.__replace_placeholders(placeholder_rows, placeholders, user_input_rows)

        return result.strip()

    def __get_parts_between_rows(self, placeholder_rows):
        parts_between_rows = []
        for row in placeholder_rows:
            if row != placeholder_rows[0] and '...' not in row:
                parts_between_rows.append(row)
        return parts_between_rows

    def __replace_placeholders(self, placeholder_rows, placeholders, user_input_rows):
        middle_part = re.findall('(?s:.*)#([\d\D]*)', '\n'.join(placeholder_rows))[0]
        if ')' in middle_part:
            return self.__handle_replacement_for_listing(placeholder_rows, middle_part[:-4],
                                                         placeholders, user_input_rows)
        elif '&&' in middle_part:
            return self.__handle_replacement_for_concatenation(placeholder_rows, middle_part[:-3],
                                                               placeholders, user_input_rows)

    def __handle_replacement_for_listing(self, placeholder_rows, middle_part, placeholders, user_input_rows):
        result = ''
        first_row = placeholder_rows[0]
        start_part = first_row.split('#')[0]
        part_before_placeholders = re.findall(r'\(.*?#', first_row)[0][1:-1]
        part_with_placeholder = re.findall(r'#.*#', first_row)[0]

        for index, row in enumerate(user_input_rows):
            if index == 0:
                result = start_part
                result += part_with_placeholder
                for placeholder in placeholders:
                    data_key = placeholder[1:-1]
                    result = result.replace(placeholder, row[data_key])
                result += middle_part
            elif index != len(user_input_rows) - 1:
                new_row = part_with_placeholder
                for placeholder in placeholders:
                    data_key = placeholder[1:-1]
                    new_row = new_row.replace(placeholder, row[data_key])
                result += part_before_placeholders + new_row + middle_part
            else:
                new_row = part_with_placeholder
                for placeholder in placeholders:
                    data_key = placeholder[1:-1]
                    new_row = new_row.replace(placeholder, row[data_key])
                end_part = middle_part.split('\n')[0]
                end_part = end_part[:-1] + ')'
                result += part_before_placeholders + new_row + end_part + '\n'

        return result

    def __handle_replacement_for_concatenation(self, placeholder_rows, middle_part, placeholders, user_input_rows):
        result = ''
        first_row = placeholder_rows[0]
        for placeholder in placeholders:
            for index, row in enumerate(user_input_rows):
                data_key = placeholder[1:-1]
                if not row[data_key]:
                    continue
                if index != len(user_input_rows)-1:
                    result += first_row.replace(placeholder, row[data_key]) + middle_part
                else:
                    result += first_row.replace(placeholder, row[data_key]) + '\n'
        return result

    def __add_parts_between_rows(self, parts, result):
        rows = result.split('\n')
        processed_rows = rows
        insertion_count = 0
        for index in range(1, len(rows)-1):
            processed_rows.insert(index + insertion_count, '\n'.join(parts))
            insertion_count += 1
        return '\n'.join(processed_rows).rstrip()
