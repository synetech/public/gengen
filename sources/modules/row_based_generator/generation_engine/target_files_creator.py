import shutil
from sources.modules.row_based_generator.key_value_storage import *
from sources.modules.row_based_generator.generation_engine.placeholder_processor import PlaceholderProcessor
from sources.utils.list_files import list_files


class TargetFilesCreator(PlaceholderProcessor):

    def create_target_files_for_entire_user_input(self, user_input, config):
        templates_directory = KeyValueStorage().selected_template_folder
        output_directory = KeyValueStorage().selected_output_folder
        output_directory = output_directory if output_directory else os.getcwd()
        root_directory_name = user_input.data[config.output_folder_name_key]
        target_directory = output_directory + os.sep + root_directory_name

        try:
            os.mkdir(target_directory)
            self.__copy_tree(templates_directory, target_directory)
        except FileExistsError:
            self.__copy_tree(templates_directory, target_directory)
        shutil.rmtree(target_directory + os.sep + 'SubTemplates')

        target_end_files = list_files(target_directory)
        for file in target_end_files:
            for key in config.header:
                if config.header[key]['id'] in file:
                    placeholder = '#' + config.header[key]['id'] + '#'
                    new_file_name = file.replace(placeholder, user_input.data[config.header[key]['id']])
                    os.rename(file, new_file_name)

            if '.config' in file:
                os.remove(file)
            file_name = file.split(os.sep)[-1]

            if file_name in config.rows_only:
                os.remove(file)

        try:
            shutil.rmtree(target_directory + os.sep + 'Templates')
        except FileNotFoundError:
            pass

    def create_target_files_for_rows_only_input(self, user_input, config):
        templates_directory = KeyValueStorage().selected_template_folder
        output_directory = KeyValueStorage().selected_output_folder
        output_directory = output_directory if output_directory else os.getcwd()
        root_directory_name = user_input.data[config.output_folder_name_key]
        target_directory = output_directory + os.sep + root_directory_name

        files = list_files(templates_directory)
        rows_only_files = [file for file in files if file.split(os.sep)[-1] in config.rows_only]
        for file in rows_only_files:
            opened_file = open(file, 'r')
            content = opened_file.read()
            opened_file.close()

            relative_file_path = file.replace(templates_directory, '')
            renamed_file_paths = []
            for row in user_input.rows:
                placeholder = self.get_all_placeholders(relative_file_path)[0]
                placeholder_value = row[placeholder[1:-1]]
                renamed_file_paths.append(relative_file_path.replace(placeholder,
                                                                     placeholder_value[0].upper()
                                                                     + placeholder_value[1:]))
            target_file_paths = [target_directory + path for path in renamed_file_paths]

            file_names = [file.split(os.sep)[-1] for file in renamed_file_paths]

            for name in zip(file_names, target_file_paths):
                target_folder = name[1].replace(name[0], '')
                try:
                    os.makedirs(target_folder)
                except FileExistsError:
                    continue

            for target_file in target_file_paths:
                try:
                    target_file = open(target_file, 'x')
                    target_file.write(content)
                    target_file.close()
                except FileExistsError:
                    continue

    @classmethod
    def __copy_tree(cls, src, dst, symlinks=False, ignore=None):

        for item in os.listdir(src):
            source = os.path.join(src, item)
            destination = os.path.join(dst, item)
            if os.path.isdir(source):
                try:
                    shutil.copytree(source, destination, symlinks, ignore)
                except FileExistsError:
                    shutil.rmtree(destination)
                    shutil.copytree(source, destination, symlinks, ignore)
            else:
                shutil.copy2(source, destination)
