class Keys:

    id = "id"
    type = "view_type"
    custom_type = 'custom_type'
    data = "data_entry"
    data_type = "data_type"
    interaction = "interaction"
    reused = "reused"
    subviews = "views"
    name = "name"
    rows = 'rows'
