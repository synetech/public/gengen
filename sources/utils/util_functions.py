import os
import sys


def read_file(file_path):
    file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, file_path))
    try:
        with open(file_path, 'r') as file:
            file_content = file.read()
        file.close()
    except FileNotFoundError:
        raise FileNotFoundError
    return file_content


def write_to_file(file_path, content):
    file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, file_path))
    with open(file_path, 'w') as file:
        file.write(str(content))
    file.close()


def get_working_directory():
    if getattr(sys, 'frozen', False):
        return os.path.dirname(sys.executable)
    else:
        return os.getcwd()
