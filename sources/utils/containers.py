import pinject

# SCREEN BUILDER MODULE COMPONENTS
from sources.modules.row_based_generator.generation_engine.row_rules_handler import RowRulesHandler
from sources.modules.row_based_generator.ui.view_model import ViewModel
from sources.modules.row_based_generator.generation_engine.generation_handler import GenerationHandler
from sources.modules.row_based_generator.generation_engine.rows_only_handler import RowsOnlyHandler
from sources.modules.row_based_generator.generation_engine.target_files_creator import TargetFilesCreator
from sources.modules.row_based_generator.generation_engine.row_input_adjuster import RowInputAdjuster
from sources.modules.row_based_generator.generation_engine.sub_placeholder_replacer import SubPlaceholderReplacer
from sources.modules.row_based_generator.row_based_generator_module import RowBasedGeneratorModule
from sources.modules.row_based_generator.ui.main_view_handler import MainViewHandler
from sources.modules.row_based_generator.ui.rows_handler import RowsHandler
from sources.modules.row_based_generator.generation_engine.placeholder_replacer import PlaceholderReplacer


OBJECTS = pinject.new_object_graph(classes=[
    RowsHandler,
    RowBasedGeneratorModule,
    ViewModel,
    MainViewHandler,
    RowInputAdjuster,
    SubPlaceholderReplacer,
    PlaceholderReplacer,
    GenerationHandler,
    TargetFilesCreator,
    RowRulesHandler,
    RowsOnlyHandler
])


class Modules:

    row_based_generator = OBJECTS.provide(RowBasedGeneratorModule)
    generation_handler = OBJECTS.provide(GenerationHandler)
