# GenGen

Fully customizable template based code generation app.

## Project setup

### Prerequisites:
- Python 3+ interpreter available in PATH
  - to confirm, run `python3` in terminal, the python interpreter should start
  - to exit the interpreter, run `exit()`
  - if not installed, go to https://www.python.org/ and download the newest version

### In terminal:
- run `sh create_venv.sh`
  - this will a create Python virtual environment with all dependency packages installed

- to run the app, first activate the virtual environment `source venv/bin/activate` (to deactivate, just run `deactivate`)
- with the activate environment, run `python3 GenGen.py`

### If using PyCharm:
NOTE: The Python virtual environment needs to be created (see the In terminal section above)
- open the project (just choose the folder in open dialog)
- go to Preferences (PyCharm -> Preferences or 'command + ,')
- select Project: gengen
- go to Project Interpreter
- on the right upper side, click on the wheel icon and select 'Show all'
- in the Project Interpreters dialog click on the '+' icon at the bottom
- select 'Existing environment' option and set the path to `bin/python/python3.*` file in the created `venv` folder
