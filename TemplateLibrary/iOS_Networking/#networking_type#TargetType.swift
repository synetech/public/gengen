//
//  #networking_type#TargetType.swift
//
//  Created by GenGen.
//

import Moya

enum #networking_type#TargetType {

    #request_case#

    var requestSpec: RequestSpec {
        switch self {
        #request_spec_case#
        }
    }
}

extension #networking_type#TargetType: TargetType {

    var baseURL: URL {
        // swiftlint:disable force_unwrapping
        return URL(string: requestSpec.baseURL)!
    }

    var path: String {
        return requestSpec.path
    }

    var method: Moya.Method {
        return requestSpec.method
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        return requestSpec.task
    }

    var headers: [String: String]? {
        return requestSpec.headers
    }
}
