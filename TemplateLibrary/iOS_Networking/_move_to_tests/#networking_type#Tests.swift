//
//  #networking_type#Tests.swift
//
//  Created by GenGen.
//

// TODO: Add testable import
// @testable import
import RxBlocking
import XCTest

class #networking_type#Tests: XCTest {

    // MARK: - Properties
    var networking = #networking_type#Impl()

    // MARK: - Setup
    override func setUp() {
        super.setUp()
        // TODO: Add setup if needed
    }

    override func tearDown() {
        super.tearDown()
        // TODO: Add tearDown if needed
    }
}

extension #networking_type#Test {

    // MARK: - Tests
    #request_test#
}
