import RealmSwift

extension List {

    static var baseValue: List {
        return List<Element>()
    }
}

extension String {

    static var baseValue: String {
        return ""
    }
}

extension Int {

    static var baseValue: Int {
        return 0
    }
}

extension Date {

    static var baseValue: Date {
        return Date()
    }
}

extension Double {

    static var baseValue: Double {
        return 0.0
    }
}

extension RealmOptional {

    static var baseValue: RealmOptional<Value> {
        RealmOptional<Value>()
    }
}
