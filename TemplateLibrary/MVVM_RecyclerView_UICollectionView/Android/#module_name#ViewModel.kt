package #package#.#module_name.lowercased#

import android.arch.lifecycle.ViewModel

// TODO: change repository argument
class #module_name#ViewModel(
    private val repository: #module_name#Repository = Mock#module_name#Repository()) : ViewModel() {

    fun getLiveData() = repository.getDataFor#module_name#()
}
