package #package#.#module_name.lowercased#

import android.arch.lifecycle.MutableLiveData

interface #module_name#Repository {

    fun getDataFor#module_name#(): MutableLiveData<List<#model_type#>>
}

class Mock#module_name#Repository: #module_name#Repository {

    override fun getDataFor#module_name#() = MutableLiveData<List<#model_type#>>()
}