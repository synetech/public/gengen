package com.gengen.test

import org.junit.Test
import org.junit.Before

class #module_name#ViewModelTests {

  lateinit var viewModel: #module_name#ViewModel

  @Before
  fun setup() {
    repositry = Mock#module_name#Repository
    viewModel = #module_name#ViewModel(repositry)
  }

  @Test
  fun test() {
    // TODO: add test content
    assert(viewModel.getLiveData().value.isNullOrEmpty())
  }
}
