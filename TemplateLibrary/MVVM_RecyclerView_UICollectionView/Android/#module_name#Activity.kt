package #package#.#module_name.lowercased#

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import #package#.R
import kotlinx.android.synthetic.main.#layout_name#_activity_content.*

class #module_name#Activity : AppCompatActivity() {

    private lateinit var viewModel: #module_name#ViewModel

    private val adapter = #module_name#Adapter(mutableListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(#module_name#ViewModel::class.java)

        configureUI()
        configureInteractions()
        configureDataObservation()
    }

    private fun configureUI() {
        setContentView(R.layout.#layout_name#_activity)
  }

    private fun configureInteractions() {
        // TODO: configure interactions
    }

    private fun configureDataObservation() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        viewModel.getLiveData().observe(this, Observer { data ->
            data?.let {
                adapter.update(data)
            }
        })
    }
}
