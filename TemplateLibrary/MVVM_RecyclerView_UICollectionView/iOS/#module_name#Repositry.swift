//
//  #module_name#Repository.swift
//
//  Created by GenGen.
//

import RxSwift

protocol #module_name#Repository {

    func getDataFor#module_name#() -> Observable<[#model_type#]>
}

class Mock#module_name#Repository: #module_name#Repository {

    func getDataFor#module_name#() -> Observable<[#model_type#]> {
        // TODO: implement
        return Observable.just([])
    }
}
