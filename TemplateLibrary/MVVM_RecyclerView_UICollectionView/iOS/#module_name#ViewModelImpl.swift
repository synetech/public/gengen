//
//  #module_name#ViewModelProtocol.swift
//
//  Created by GenGen.
//

import RxCocoa
import RxSwift

protocol #module_name#ViewModelProtocol {

    typealias Repository = #module_name#Repository
    typealias Model = #model_type#

    func getModel() -> Observable<[Model]>
}

class #module_name#ViewModel {

    // MARK: - Properties
    private let repository: Repository
    private let model = BehaviorRelay<[Model]>(value: [])

    // MARK: - Init
    init(repository: Repository) {
        self.repository = repository
    }
}

extension #module_name#ViewModel: #module_name#ViewModelProtocol {

    // MARK: - Data providing
    func getModel() -> Observable<[Model]> {
        return model.asObservable()
            .filter { !$0.isEmpty }
    }
}
