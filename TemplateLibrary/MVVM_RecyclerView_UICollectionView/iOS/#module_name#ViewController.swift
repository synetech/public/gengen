//
//  #module_name#ViewController.swift
//
//  Created by GenGen.
//

import Stevia
import RxSwift

class #module_name#ViewController: UIViewController {

    #view_controller_aliases#

    private lazy var coll#module_name# = #module_name#CollectionView(layout: #module_name#CollectionView.Layout())

    #view_controller_properties#

    #view_controller_init#

    #view_controller_lifecycle#

    #view_controller_setup#
}

// MARK: - Data binding
extension #module_name#ViewController {

    private func bindData() {
        let dataSource = coll#module_name#.getDataSource()
        viewModel.getModel()
            .map { $0.toSections() }
            .bind(to: coll#module_name#.rx
                .items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}

// MARK: - Delegation handling
extension #module_name#ViewController: UICollectionViewDelegate {

    private func setupCollectionViewDelegate() {
        coll#module_name#.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }
}
