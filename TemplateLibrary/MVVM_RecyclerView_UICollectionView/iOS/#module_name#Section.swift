//
//  #model_type#Section.swift
//
//  Created by GenGen.
//

import RxDataSources

struct #model_type#Section {

    var header: String
    var items: [Item]
}

extension #model_type#Section: AnimatableSectionModelType {

    typealias Item = #model_type#

    init(original: #model_type#Section, items: [Item]) {
        self = original
        self.items = items
    }
}

extension #model_type#Section: IdentifiableType {

    typealias Identity = String

    var identity: Identity { return header }
}

extension #model_type#: IdentifiableType {

    typealias Identity = String

    var identity: Identity { return "SPECIFY IDENTITY STRING" } // TODO: identity
}

extension Array where Element == #model_type# {

    func toSections(withHeader header: String = "") -> [#model_type#Section] {
        return [#model_type#Section(header: header, items: self)]
    }
}