import RxBlocking
import XCTest
@testable import ADD_TESTABLE_IMPORT

class #module_name#ViewModelTests: XCTestCase {

    var viewModel: SpeakersListViewModelProtocol!

    override func setUp() {
        let repository =  Mock#module_name#Repository()
        viewModel = #module_name#ViewModel(repository: repository)

    }

    func testExample() {
        let result = try! viewModel.getModel().toBlocking().first()!
        assert(result.isEmpty)
    }
}
