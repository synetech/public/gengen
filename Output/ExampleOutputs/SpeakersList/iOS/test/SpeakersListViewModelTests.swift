import RxBlocking
import XCTest
@testable import ADD_TESTABLE_IMPORT

class SpeakersListViewModelTests: XCTestCase {

    var viewModel: SpeakersListViewModelProtocol!

    override func setUp() {
        let repository =  MockSpeakersListRepository()
        viewModel = SpeakersListViewModel(repository: repository)

    }

    func testExample() {
        let result = try! viewModel.getModel().toBlocking().first()!
        assert(result.isEmpty)
    }
}
