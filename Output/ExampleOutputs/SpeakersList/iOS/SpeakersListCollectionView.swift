//
//  SpeakersListCollectionView.swift
//
//  Created by GenGen.
//

import RxDataSources
import UIKit

class SpeakersListCollectionView: UICollectionView {

    // MARK: - Aliases
    typealias Cell = SpeakersListCell
    typealias CellModel = Cell.Model
    typealias CellDelegate = SpeakersListCellDelegate
    typealias Layout = UICollectionViewFlowLayout
    typealias RxDataSourceAnimated = RxCollectionViewSectionedAnimatedDataSource

    private let cellTypeString = String(describing: Cell.self)

    // MARK: - Init
    init(layout: Layout) {
        super.init(frame: .zero, collectionViewLayout: layout)

        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    private func setupView() {
        register(Cell.self, forCellWithReuseIdentifier: cellTypeString)
    }
}

extension SpeakersListCollectionView {

    func getDataSource(delegate: CellDelegate? = nil) -> RxDataSourceAnimated<SpeakerSection> {
    return RxDataSourceAnimated<SpeakerSection>(configureCell: { [weak self] _, collectionView, indexPath, model in
            self?.getCellConfiguration(delegate: delegate,
                                       collectionView: collectionView,
                                       indexPath: indexPath,
                                       model: model) ?? UICollectionViewCell()
        })
    }

    private func getCellConfiguration(delegate: CellDelegate?,
                                      collectionView: UICollectionView,
                                      indexPath: IndexPath,
                                      model: CellModel) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellTypeString,
                                                      for: indexPath) as? Cell ?? Cell()
        cell.setup(model: model)
        cell.setDelegate(delegate)
        return cell
    }
}

protocol SpeakersListCellDelegate : class {}
