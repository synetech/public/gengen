//
//  SpeakerSection.swift
//
//  Created by GenGen.
//

import RxDataSources

struct SpeakerSection {

    var header: String
    var items: [Item]
}

extension SpeakerSection: AnimatableSectionModelType {

    typealias Item = Speaker

    init(original: SpeakerSection, items: [Item]) {
        self = original
        self.items = items
    }
}

extension SpeakerSection: IdentifiableType {

    typealias Identity = String

    var identity: Identity { return header }
}

extension Speaker: IdentifiableType {

    typealias Identity = String

    var identity: Identity { return "SPECIFY IDENTITY STRING" } // TODO: identity
}

extension Array where Element == Speaker {

    func toSections(withHeader header: String = "") -> [SpeakerSection] {
        return [SpeakerSection(header: header, items: self)]
    }
}