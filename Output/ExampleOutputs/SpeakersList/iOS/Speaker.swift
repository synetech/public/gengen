//
//  Speaker.swift
//
//  Created by GenGen.
//

struct Speaker {

    let photoURL: String
    let name: String
    let topic: String
    let time: String
}

extension Speaker: Equatable {

    static func == (lhs: Speaker, rhs: Speaker) -> Bool {
        return lhs.photoURL == rhs.photoURL
            && lhs.name == rhs.name
            && lhs.topic == rhs.topic
            && lhs.time == rhs.time
    }
}