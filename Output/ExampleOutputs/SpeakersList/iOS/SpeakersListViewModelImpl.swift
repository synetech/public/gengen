//
//  SpeakersListViewModelProtocol.swift
//
//  Created by GenGen.
//

import RxCocoa
import RxSwift

protocol SpeakersListViewModelProtocol {

    typealias Repository = SpeakersListRepository
    typealias Model = Speaker

    func getModel() -> Observable<[Model]>
}

class SpeakersListViewModel {

    // MARK: - Properties
    private let repository: Repository
    private let model = BehaviorRelay<[Model]>(value: [])

    // MARK: - Init
    init(repository: Repository) {
        self.repository = repository
    }
}

extension SpeakersListViewModel: SpeakersListViewModelProtocol {

    // MARK: - Data providing
    func getModel() -> Observable<[Model]> {
        return model.asObservable()
            .filter { !$0.isEmpty }
    }
}
