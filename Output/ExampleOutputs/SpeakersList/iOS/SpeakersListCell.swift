//
//  SpeakersListCell.swift
//
//  Created by GenGen.
//

import Stevia

class SpeakersListCell: UICollectionViewCell {

    // MARK: - Aliases
    typealias Delegate = SpeakersListCellDelegate
    typealias Model = Speaker

    // MARK: - Views
    lazy var photoImageView = UIImageView()
    lazy var nameTextView = UITextView()
    lazy var topicTextView = UITextView()
    lazy var timeTextView = UITextView()
    lazy var saveButton = UIButton()

    // MARK: - Properties
    private var delegate: Delegate?

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    private func setupView() {
        // TODO: add setup
    }

    private func layoutViews() {
        layout(|-photoImageView-|,
               0,
               |-nameTextView-|,
               0,
               |-topicTextView-|,
               0,
               |-timeTextView-|,
               0,
               |-saveButton-|)
    }
}

extension SpeakersListCell {

    func setup(model: Model) {
        // TODO: set model values to value properties
        // photoImageView.value = model.photoURL
        // nameTextView.value = model.name
        // topicTextView.value = model.topic
        // timeTextView.value = model.time
    }

    func setDelegate(_ delegate: Delegate?) {
        self.delegate = delegate
    }
}
