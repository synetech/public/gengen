//
//  SpeakersListRepository.swift
//
//  Created by GenGen.
//

import RxSwift

protocol SpeakersListRepository {

    func getDataForSpeakersList() -> Observable<[Speaker]>
}

class MockSpeakersListRepository: SpeakersListRepository {

    func getDataForSpeakersList() -> Observable<[Speaker]> {
        // TODO: implement
        return Observable.just([])
    }
}
