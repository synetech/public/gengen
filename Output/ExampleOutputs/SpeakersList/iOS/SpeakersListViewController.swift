//
//  SpeakersListViewController.swift
//
//  Created by GenGen.
//

import Stevia
import RxSwift

class SpeakersListViewController: UIViewController {

    // MARK: - Aliases
    typealias ViewModel = SpeakersListViewModel

    private lazy var collSpeakersList = SpeakersListCollectionView(layout: SpeakersListCollectionView.Layout())

    // MARK: - Properties
    private let viewModel: ViewModel

    private let disposeBag = DisposeBag()

    // MARK: - Init
    init(viewModel: ViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        bindData()
    }

    // MARK: - Setup
    private func setupViews() {
        view.sv(collSpeakersList)
        collSpeakersList.fillContainer()
        setupCollectionViewDelegate()
    }
}

// MARK: - Data binding
extension SpeakersListViewController {

    private func bindData() {
        let dataSource = collSpeakersList.getDataSource()
        viewModel.getModel()
            .map { $0.toSections() }
            .bind(to: collSpeakersList.rx
                .items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}

// MARK: - Delegation handling
extension SpeakersListViewController: UICollectionViewDelegate {

    private func setupCollectionViewDelegate() {
        collSpeakersList.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }
}
