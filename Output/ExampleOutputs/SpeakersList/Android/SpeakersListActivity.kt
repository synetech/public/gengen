package com.synetech.test.speakerslist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.synetech.test.R
import kotlinx.android.synthetic.main.speakers_list_activity_content.*

class SpeakersListActivity : AppCompatActivity() {

    private lateinit var viewModel: SpeakersListViewModel

    private val adapter = SpeakersListAdapter(mutableListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SpeakersListViewModel::class.java)

        configureUI()
        configureInteractions()
        configureDataObservation()
    }

    private fun configureUI() {
        setContentView(R.layout.speakers_list_activity)
  }

    private fun configureInteractions() {
        // TODO: configure interactions
    }

    private fun configureDataObservation() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        viewModel.getLiveData().observe(this, Observer { data ->
            data?.let {
                adapter.update(data)
            }
        })
    }
}
