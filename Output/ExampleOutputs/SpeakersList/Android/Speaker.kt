package com.synetech.test.speakerslist

data class Speaker(val photoURL: String = "photo.url",
     val name: String = "John Doe",
     val topic: String = "Code generation",
     val time: String = "10:30")
