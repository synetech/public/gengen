package com.synetech.test.speakerslist

import android.arch.lifecycle.ViewModel

// TODO: change repository argument
class SpeakersListViewModel(
    private val repository: SpeakersListRepository = MockSpeakersListRepository()) : ViewModel() {

    fun getLiveData() = repository.getDataForSpeakersList()
}
