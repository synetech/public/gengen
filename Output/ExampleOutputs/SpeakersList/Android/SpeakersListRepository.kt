package com.synetech.test.speakerslist

import android.arch.lifecycle.MutableLiveData

interface SpeakersListRepository {

    fun getDataForSpeakersList(): MutableLiveData<List<Speaker>>
}

class MockSpeakersListRepository: SpeakersListRepository {

    override fun getDataForSpeakersList() = MutableLiveData<List<Speaker>>()
}