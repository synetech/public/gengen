package com.synetech.test.speakerslist

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.synetech.test.R
import com.synetech.test.app.inflate
import kotlinx.android.synthetic.main.speakers_list_activity_list_item.view.*

class SpeakersListAdapter(private val data: MutableList<Speaker>)
  : RecyclerView.Adapter<SpeakersListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.speakers_list_activity))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount() = data.size

    fun update(model: List<Speaker>) {
        this.data.clear()
        this.data.addAll(model)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private lateinit var data: Speaker

        init { itemView.setOnClickListener(this) }

        fun bind(data: Speaker) {
            this.data = data
            // TODO: adjust data values assignment
            // itemView.photoImageView = data.photoURL
            // itemView.nameTextView = data.name
            // itemView.topicTextView = data.topic
            // itemView.timeTextView = data.time
            // itemView.saveButton = data.
        }

        override fun onClick(p0: View?) {
            // TODO: handle onClick
        }
    }
}