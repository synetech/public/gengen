//
//  ProductAPIEntity.swift
//
//  Created by GenGen.
//

struct ProductAPIEntity: Codable {

    let productID: String
    let name: String
    let desc: String?
    let price: PriceAPIEntity
    let colors: [String]

    enum CodingKeys: String, CodingKey {
        case productID = "Id"
        case name = "Name"
        case desc = "Description"
        case price = "Price"
        case colors = "Colors"
    }
}
