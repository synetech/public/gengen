//
//  PriceAPIEntity.swift
//
//  Created by GenGen.
//

struct PriceAPIEntity: Codable {

    let price: Double
    let currency: String
    let discountAmount: Int?

    enum CodingKeys: String, CodingKey {
        case price = "Price"
        case currency = "Currency"
        case discountAmount = "Discount"
    }
}
