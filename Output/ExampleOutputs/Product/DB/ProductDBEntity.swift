//
//  ProductDBEntity.swift
//
//  Created by GenGen.
//

import RealmSwift

class ProductDBEntity: Object {

    // MARK: - Properties
    @objc dynamic var productID: String = .baseValue
    @objc dynamic var name: String = .baseValue
    @objc dynamic var desc: String? = .baseValue
    @objc dynamic var priceDBEntity: PriceDBEntity = PriceDBEntity()
    var colorsList: List<String> = .baseValue

    override class func primaryKey() -> String? {
        return "productID"
    }

    // MARK: - Init
    convenience init(from entity: ProductEntity) {
        self.init()
        self.productID = entity.productID
        self.name = entity.name
        self.desc = entity.desc
        self.priceDBEntity = PriceDBEntity(from: entity.priceEntity)
        entity.colors.forEach { self.colorsList.append($0) }
    }
}

extension ProductDBEntity: ProductEntity {

    var colors: [String] {
        return colorsList.map { $0 }
    }

    var priceEntity: PriceEntity {
        return priceDBEntity
    }
}
