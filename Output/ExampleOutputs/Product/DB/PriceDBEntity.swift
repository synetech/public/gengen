//
//  PriceDBEntity.swift
//
//  Created by GenGen.
//

import RealmSwift

class PriceDBEntity: Object {

    // MARK: - Properties
    @objc dynamic var price: Double = .baseValue
    @objc dynamic var currency: String = .baseValue
    var discountAmountOptional: RealmOptional<Int> = .baseValue

    // MARK: - Init
    convenience init(from entity: PriceEntity) {
        self.init()
        self.price = entity.price
        self.currency = entity.currency
        self.discountAmountOptional.value = entity.discountAmount

    }
}

extension PriceDBEntity: PriceEntity {





    var discountAmount: Int? {
        discountAmountOptional.value
    }
}
