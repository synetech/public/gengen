//
//  PriceEntity.swift
//
//  Created by GenGen.
//

protocol PriceEntity {

    var price: Double { get }
    var currency: String { get }
    var discountAmount: Int? { get }

}
