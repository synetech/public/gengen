//
//  ProductModel.swift
//
//  Created by GenGen.
//

protocol ProductEntity {

    var productID: String { get }
    var name: String { get }
    var desc: String? { get }
    var colors: [String] { get }
    var priceEntity: PriceEntity { get }
}
