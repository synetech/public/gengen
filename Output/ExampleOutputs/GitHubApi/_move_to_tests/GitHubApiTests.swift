//
//  GitHubApiTests.swift
//
//  Created by GenGen.
//

// TODO: Add testable import
// @testable import
import RxBlocking
import XCTest

class GitHubApiTests: XCTest {

    // MARK: - Properties
    var networking = GitHubApiImpl()

    // MARK: - Setup
    override func setUp() {
        super.setUp()
        // TODO: Add setup if needed
    }

    override func tearDown() {
        super.tearDown()
        // TODO: Add tearDown if needed
    }
}

extension GitHubApiTest {

    // MARK: - Tests
    func test_RetrieveTeamMembers() {
        let result = networking.retrieveTeamMembers().toBlocking().first
        XCTAssertNotNil(result)
        // TODO: Add additional checks if needed
    }

    func test_RetrieveMember() {
        let result = networking.retrieveMember().toBlocking().first
        XCTAssertNotNil(result)
        // TODO: Add additional checks if needed
    }
}
