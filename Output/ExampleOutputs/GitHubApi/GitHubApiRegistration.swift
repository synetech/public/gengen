//
//  GitHubApiRegistration.swift
//
//  Created by GenGen.
//

import Swinject

extension DataSourceDependencies {

    static func registerGitHubApi(to container: Container) {
        container.register(GitHubApi.self) { r in
            GitHubApiImpl()
        }
    }
}
