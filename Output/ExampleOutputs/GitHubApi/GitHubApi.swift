//
//  GitHubApi.swift
//
//  Created by GenGen.
//

import RxSwift

protocol GitHubApi {

    func retrieveTeamMembers() -> Single<[Member]>
    func retrieveMember() -> Single<Member>
}
