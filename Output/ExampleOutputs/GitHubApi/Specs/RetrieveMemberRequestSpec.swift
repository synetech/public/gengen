//
//  RetrieveMemberRequestSpec.swift
//
//  Created by GenGen.
//

import Moya

struct RetrieveMemberRequestSpec: RequestSpec {

    var baseURL: String = ""
    var path: String = "users/%@"
    var method: Moya.Method = .get
    var task: Task = .requestPlain
    var headers: [String: String] = [:]

    // TODO: customize init if needed
    init(baseURL: String) {
        self.baseURL = baseURL
    }
}
