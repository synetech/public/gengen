//
//  GitHubApiTargetType.swift
//
//  Created by GenGen.
//

import Moya

enum GitHubApiTargetType {

    case retrieveTeamMembers(RetrieveTeamMembersRequestSpec)
    case retrieveMember(RetrieveMemberRequestSpec)

    var requestSpec: RequestSpec {
        switch self {
        case .retrieveTeamMembers(let spec):
            return spec
        case .retrieveMember(let spec):
            return spec
        }
    }
}

extension GitHubApiTargetType: TargetType {

    var baseURL: URL {
        // swiftlint:disable force_unwrapping
        return URL(string: requestSpec.baseURL)!
    }

    var path: String {
        return requestSpec.path
    }

    var method: Moya.Method {
        return requestSpec.method
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        return requestSpec.task
    }

    var headers: [String: String]? {
        return requestSpec.headers
    }
}
