//
//  GitHubApiImpl.swift
//
//  Created by GenGen.
//

import Moya
import RxSwift

class GitHubApiImpl {

    // MARK: - Aliases
    typealias TargetType = GitHubApiTargetType

    // MARK: - Properties
    private let provider: MoyaProvider<TargetType> = {
        return MoyaProvider(endpointClosure: MoyaProvider.defaultEndpointMapping,
                            stubClosure: MoyaProvider.neverStub,
                            manager: MoyaProvider<TargetType>.defaultAlamofireManager(),
                            plugins: [NetworkLoggerPlugin(cURL: true)],
                            trackInflights: false)
    }()

    private let baseURL = "https://api.github.com"

    // MARK: - Init
    init() {}
}

extension GitHubApiImpl: GitHubApi {

    func retrieveTeamMembers() -> Single<[Member]> {
        let spec = RetrieveTeamMembersRequestSpec(baseURL: baseURL)
        return provider.rx.request(.retrieveTeamMembers(spec))
            .mapArray([Member].self)
    }

    func retrieveMember() -> Single<Member> {
        let spec = RetrieveMemberRequestSpec(baseURL: baseURL)
        return provider.rx.request(.retrieveMember(spec))
            .map(Member.self)
    }
}
